# Описание
Javascript приложение, реализующее классическую игру 2048


## Переменные gitlab-ci
|  Наименование |  Пример | Описание  |
|---|---|---|
|  IMAGE_NAME |${CI_REGISTRY}/my-app-here/2048-game   | адрес вашего проекта для публикации docker образ  |
|  IP_ADDR_APP |game-2048.test:81   | DNS-имя или IP-адрес вашего балансировщика при деплое через Ansible. Параметр для информации при окончании сборки  |
|  DEPLOY_TO |dev   | Каталог inventory для ansible  |
|  YANDEX_CLUSTER_NAME |k8s-game   | Имя кластера Managed Service for Kubernetes в Yandex Cloud  |
|  HELM_GIT_REPO |git@gitlab.com:my-k8s-here/game2048.git   | Репозиторий с helm chart для деплоя приложения  |
|  TERRAFORM_GIT_REPO |git@gitlab.com:my-terraform-here/kubernetes-cluster.git | Репозиторий с манифестом для Terraform  |
|  K8S_WEBSITE_NAME |https://game.devops.com   | Параметр для информации адреса публикации конечного приложения  |


## Сборка и тестирование
### Локальное тестирование
Пример сборки и запуска локально.

Установите node 16
```shell
curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt install nodejs -y
git clone git@gitlab.com:my-app-here/2048-game.git
```
Перейдите в папку 2048-game `cd 2048-game` и выполните следующие команды:
```shell
npm install --include=dev
npm run build
```
Запустите приложение. Оно будет доступно по localhost:8081
```shell
npm start
```
### Deploy с помощью Ansible и gitlab-ci
- Роли ansible:
    - Docker `https://gitlab.com/my-ansible-roles-here/docker`
    - Haproxy `https://gitlab.com/my-ansible-roles-here/haproxy`

Образ docker собирается через gitlab-runner с типом docker. Образ сборщика `docker:dind`

Deploy осуществляется через gitlab-runner с типом shell, который вызывает ansible-playbook. Для оптимизации процесса, на сервере с gitlab-runner выполните следующие действия:
* Перейдите в сессию пользователя gitlab-runner `sudo su gitlab-runner`
* Сгенериуйте ключ `ssh-keygen`
* Открытую часть ( cat ~/.ssh/your_key.pub) скопируйте на удаленный сервер в файл ~/.ssh/authorized_keys
* Добавьте пользователя в группу sudo `sudo adduser gitlab-runner sudo`
* Добавьте право на выполнение sudo без запроса пароля . Раскоментируйте или добавьте строчку в файл /etc/sudoers `%sudo   ALL=(ALL:ALL) ALL`

- Порт приложения по умолчанию: 8081
- Порт Haproxy по умолчанию: 81
- Сборка образа публикуется в репозиторий GitLab.

- Отредактируйте файл `.cicd/inventories/hosts`, указав список серверов:
    - `app`: группа серверов с приложением
    - `web`: сервер с HAProxy

- CI/CD `.gitlab-ci.yml`:
    - Измените значение переменной `IP_ADDR_APP` - адрес балансировщика HAProxy, параметр для информации

### Deploy в k8s кластер (Yandex Cloud) с помощью Helm Chart
- Список репозиториев:
  - Teraform deploy for k8s https://gitlab.com/my-terraform-here/kubernetes-cluster
  - Helm chart https://gitlab.com/my-k8s-here/game2048/-/tree/main/app_helm_2048?ref_type=heads
- Для успешного прохождения pipeline убедитесь, что на вашем gitlab-runner'e произведена инициализация интерфейса командной строки Yandex Cloud https://cloud.yandex.ru/ru/docs/cli/quickstart#install . Также файл `yc` поместите в папку bin и выдайте права на исполнение. `mv /path/to/yc /bin/yc && chmod +x /bin/yc`
- Инфраструктура подготавливается через Terraform. Инструкция по началу работы с Terraform от Yandex Cloud https://cloud.yandex.ru/ru/docs/tutorials/infrastructure-management/terraform-quickstart . 
- Перед запуском pipeline необходимо под пользователем gitlab-runner выполнить следующие действия:
  - создать папку и перейти в нее mkdir `~/terraform && cd ~/terraform`
  - Создать ключ, где UserLogin1 - ваше имя учетной записи в YandexCloud
  ``` shell
  yc iam key create \
  --service-account-name UserLogin1 \
  --output key.json
  ```
  - Добавить в файл `~/.terraformrc` следующие данные:
  ```
  provider_installation {
    network_mirror {
      url = "https://terraform-mirror.yandexcloud.net/"
      include = ["registry.terraform.io/*/*"]
    }
    direct {
      exclude = ["registry.terraform.io/*/*"]
    }
  }
  ```

- Перед деплоем проверьте переменные в файле values.yaml https://gitlab.com/my-k8s-here/game2048/-/blob/main/app_helm_2048/values.yaml?ref_type=heads
    - image в процессе деплоя будет заменен на `${IMAGE_NAME}:${CI_COMMIT_SHORT_SHA}`
    - hostname_dns - это A-запись в DNS, которая ссылается на LoadBalancer
```
game:
  container:
    image: %%%IMAGE_NAME_AND_TAG%%%
    containerPort: 8081
  replica:
    count: 3
  ingress:
    hostname_dns: game.devops.com
```
- После успешного деплоя добавьте А-запись `game` для зоны `devops.com.`, которая ссылается на балансировщик. 
![Alt text](docs/image.png)
- Для проверки работоспособности приложения необходимо добавить DNS сервер Yandex в список DNS:
![Alt text](docs/image-1.png)
- Перейдите по адресу приложения https://game.devops.com
![Alt text](docs/image2.png)

# Основной процесс Build & Deploy
## Dev контур
- Установка ansible-galaxy ролей
- Замена имени и тэга образа для sh скрипта запуска службы
- Установка docker'a на сервера приложений
- Установка haproxy на web сервер
- Настройка балансировки haproxy. В роли haproxy запускается задача, которая собирает все ip-адреса серверов из группы с серверами приложений (test) и использует их в конфигурационном файле haproxy.cfg
- Копирование sh скриптов для управление службой 2048.service
- Включение и запуск службы

## Prod контур
- Инициализация подключения к кластеру k8s в Yandex Cloud через утилиту yc
- Установка\обновление Nginx Ingress контроллер с помощью Helm Chart
- Установка\обновление приложения с помощью Helm Chart

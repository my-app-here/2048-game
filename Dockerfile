# Этап 1: Сборка артефактов
FROM node:16-slim as builder

WORKDIR /app
COPY package*.json ./
RUN npm install --include=prod
COPY . .
RUN npm run build

# Этап 2: Финальный образ
FROM node:16-alpine

WORKDIR /app
RUN mkdir -p /app/src
# Копируем артефакты сборки из предыдущего этапа
COPY --from=builder /app/dist ./dist
COPY package*.json webpack.*.js ./
COPY ./src ./src


# Устанавливаем только production зависимости
RUN npm install --include=prod

EXPOSE 8081

CMD ["npm", "start"]

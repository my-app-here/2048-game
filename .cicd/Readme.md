# Требуемые роли
- docker
- haproxy

# Переменные
|  Наименование переменной | Значение  |   |
|---|---|---|
|2048_path_to_scripts|Каталог, в который будут скопированы sh скрипты для старт и остановки контейнера. Также данный путь используется в службе 2048.service|
|docker_install|Установка docker на сервер|
|haproxy_setting_balancer|Производить настройку балансировщика. Результатом задачи будет копирование файла haprtoxy.cfg с заполненными адресами серверов с приложениями|
